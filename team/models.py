from django.db import models


# Create your models here
# Create your models here
from users.models import User

#
# class nwseditor(models.Model):
#     editorid = models.IntegerField(20, db_column='editorid', primary_key=True)
#     intra_id = models.IntegerField()
#     tocus = models.TextField()
#     titel = models.CharField(max_length=255)
#     summary = models.TextField()
#     summaryimage = models.CharField(max_length=255)
#     fd_date = models.IntegerField()
#     fd_time = models.IntegerField()
#     datepublished = models.DateField()
#     userpublished = models.IntegerField()
#     datemodified = models.DateField()
#     usermodified = models.IntegerField()
#     datecreated = models.DateField()
#     usercreated = models.IntegerField()
#     publishrss = models.IntegerField()
#     dataid = models.IntegerField()
#     categorie = models.IntegerField()
#     dateexpired = models.DateField()
#     allow_reactions = models.IntegerField()
#     hide_reactions = models.IntegerField()
#     send_notification = models.IntegerField()
#     externalid = models.FloatField()
#     isvacature = models.IntegerField()
#     link = models.CharField(max_length=255)
#     isSpotlight = models.IntegerField()
#     viewCount = models.IntegerField()
#     isRead = models.IntegerField()
#
#     class Meta:
#         db_table = "nwseditor"p


class Tmteamsusers(models.Model):
    teamid = models.IntegerField()
    userid = models.IntegerField(primary_key=True)
    minimizewindow = models.SmallIntegerField(db_column='minimizeWindow')  # Field name made lowercase.
    isadministrator = models.SmallIntegerField(db_column='isAdministrator')  # Field name made lowercase.
    intranetid = models.IntegerField()
    isactive = models.PositiveSmallIntegerField(db_column='isActive')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'tmteamsusers'
        unique_together = (('userid', 'teamid'),)

    def __str__(self):
        return str(self.__dict__)


class Tmteams(models.Model):
    teamid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    shoutboxtitle = models.CharField(db_column='shoutboxTitle', max_length=100, blank=True, null=True)  # Field name made lowercase.
    message = models.TextField(blank=True, null=True)
    groupid = models.IntegerField()
    intranetid = models.IntegerField()
    newscategoryid = models.PositiveIntegerField(db_column='newsCategoryId', blank=True, null=True)  # Field name made lowercase.
    siteid = models.PositiveIntegerField(db_column='siteId', blank=True, null=True)  # Field name made lowercase.
    typeid = models.PositiveSmallIntegerField(db_column='typeId', blank=True, null=True)  # Field name made lowercase.
    banner = models.CharField(max_length=255)
    usercreated = models.PositiveIntegerField()
    datecreated = models.DateTimeField(blank=True, null=True)
    usermodified = models.PositiveIntegerField()
    datemodified = models.DateTimeField(blank=True, null=True)
    shortname = models.CharField(max_length=20, blank=True, null=True)
    hide_in_list = models.PositiveSmallIntegerField(blank=True, null=True)
    email = models.CharField(max_length=30, blank=True, null=True)
    publicurl = models.CharField(db_column='publicURL', max_length=100, blank=True, null=True)  # Field name made lowercase.
    zindex = models.IntegerField(db_column='zIndex')  # Field name made lowercase.
    profileid = models.IntegerField(blank=True, null=True)
    private = models.PositiveSmallIntegerField(blank=True, null=True)
    teamworkappconfig = models.CharField(max_length=200, blank=True, null=True)
    newsnotification = models.PositiveSmallIntegerField(blank=True, null=True)
    wikinotification = models.PositiveSmallIntegerField(blank=True, null=True)
    shoutboxnotification = models.PositiveSmallIntegerField(blank=True, null=True)
    wikicollection = models.TextField(blank=True, null=True)
    wikicollectionidlist = models.CharField(db_column='wikicollectionIdList', max_length=250, blank=True, null=True)  # Field name made lowercase.
    hassearch = models.SmallIntegerField(db_column='hasSearch', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'tmteams'
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template.loader import get_template
from django.db import connection
from django.template import loader, Context
from news.models import nwseditor
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from news.forms import CUserForm
import json

# Create your views here.

from django.contrib.auth.decorators import login_required


@login_required
def getTeam(request, teamid):
    #news = nwseditor.objects.all()
    context = {'teamid': teamid}
    return render(request, 'admin.html', context)


def user_login(request):
    if request.user.is_authenticated:
        query = "select teamid from tmteamsusers where userid=" + str(request.user.ID)
        cursor = connection.cursor()
        cursor.execute(query, [])
        row = cursor.fetchone()
        return HttpResponseRedirect('/' + str(row[0]))

    if request.method == "POST":
        validate_loginform = CUserForm(request.POST)
        if validate_loginform.is_valid():
            user = authenticate(request, username=request.POST.get('username'), password=request.POST.get('password'))
            if user:
                login(request, user)
                query = "select teamid from tmteamsusers where userid=" + str(user.ID)
                cursor = connection.cursor()
                cursor.execute(query, [])
                row = cursor.fetchone()
                data = {'error': False, 'response': 'loggedin successfully', 'url': '/' + str(row[0])}
                return HttpResponse(json.dumps(data))
        else:
            data = {'error': True, 'response': validate_loginform.errors}
            return HttpResponse(json.dumps(data))
    return render(request, 'user_login.html')

def team_login(request, teamid):
    context = {'teamid': teamid}
    return render(request, 'login.html', context)


def checkuser(request, teamid):

    if request.method == "POST":
        if request.POST.get('username') and request.POST.get('password'):
            users = [(request.POST.get('username')
                      , request.POST.get('password'))
                     ]
            query = "Select * from allmodusers " \
                    "where username=" + request.POST.get('username') + " and pw = " + request.POST.get('password')
            print(query, "******")
            cursor = connection.cursor()
            cursor.executemany(query, users)
            row = cursor.fetchall()
            result = cursor.rowcount
            print (result, "***********************")
            if result:
                for user in row:
                    request.session['userid'] = user.id
                return HttpResponseRedirect('/' + teamid)
            else:
                return HttpResponseRedirect('/' + teamid + '/login')
        else:
            return HttpResponseRedirect('/' + teamid + '/login')

			

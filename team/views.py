import json

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import render
from django.views import View

from team.models import Tmteams, Tmteamsusers
from users.models import User
from utils.mixins import TestIfUserInTeam


def user_in_team(calling_function):
    def check(request, team_id, *args, **kwargs):
        try:
            team = Tmteams.objects.get(teamid=team_id)
            if team.private:
                user = User.objects.get(id=request.user.id)
                if not Tmteamsusers.objects.filter(teamid=team_id, userid=request.user.id,
                                                   intranetid=user.intra_id).exists():
                    data = {'error': True, 'response': 'Not a member of this team or intranet.', 'url': '/'}
                    return HttpResponse(json.dumps(data))
        except Tmteams.DoesNotExist:
            data = {'error': True, 'response': 'Team doesn\'t exist.', 'url': '/'}
            return HttpResponse(json.dumps(data))

        return calling_function(request, team_id, *args, **kwargs)

    return check


@user_in_team
@login_required
def getTeam(request, team_id):
    # try:
    #     team = Tmteams.objects.get(teamid=team_id)
    #     team_name = team.name
    #     if team.private:
    #         user = User.objects.get(id=request.user.id)
    #         if not Tmteamsusers.objects.filter(teamid=team_id, userid=request.user.id,
    #                                            intranetid=user.intra_id).exists():
    #             data = {'error': True, 'response': 'Not a member of this team or intranet.', 'url': '/'}
    #             return HttpResponse(json.dumps(data))
    # except Tmteams.DoesNotExist:
    #     data = {'error': True, 'response': 'Team doesn\'t exist.', 'url': '/'}
    #     return HttpResponse(json.dumps(data))

    context = {'teamid': team_id, 'teamname': "test"}
    return render(request, 'admin.html', context)


class TestView(LoginRequiredMixin, TestIfUserInTeam, View):
    def get(self, request, team_id):
        context = {'teamid': team_id, 'teamname': "test"}
        return render(request, 'admin.html', context)

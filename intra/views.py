from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views import View

from team.models import Tmteamsusers, Tmteams
from users.models import User


class IndexView(LoginRequiredMixin, View):
    def get(self, request):
        user = User.objects.get(id=request.user.id)
        user_teams = Tmteamsusers.objects.filter(userid=user.id)
        teams = []
        for team in user_teams:
            try:
                print(str(team.teamid))
                team_obj = Tmteams.objects.get(teamid=str(team.teamid))
                teams.append(team_obj)
            except Tmteams.DoesNotExist:
                print("Doesn't exist")

        context = {'teams': teams}
        return render(request, 'index.html', context)

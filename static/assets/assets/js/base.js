$(document).ready(function(){
	/* Initialization - Select*/
	 $('select').material_select();
	 
	 /* Initialization - Date*/
	   $(".datepicker").pickadate({
		selectMonths: !0,
		selectYears: 200,
		format: "dd-mm-yyyy",
		editable: true
	  })
	  
	  $('#newsarticles').pageMe({
		pagerSelector:'#myPager',
		activeColor: 'black',
		prevText:'Anterior',
		nextText:'Siguiente',
		showPrevNext:true,
		hidePageNumbers:false,
		perPage:10
	  });
	  
	  /* Date picker*/
	  $(document).on('click', '.datepicker-icon', function() {
		var $input = $(this).parent().find('.datepicker').pickadate({
		  selectMonths: !0,
		  selectYears: 200,
		  format: "dd-mm-yyyy"
		});

		var picker = $input.pickadate('picker');
		picker.open();

		$(this).parent().find('.datepicker').addClass('active-picker');

		picker.on('render', function() {
		  var _date = this.get();
		  $('.active-picker').val(_date);
		});

		picker.on('close', function() {
		  $('.active-picker').removeClass('active-picker');
		});
	  });
	  
	$(".time").focusout(function(){
		$(this).context.value = correctTime($(this).context.value);
	});  
  
	//Requires Globalize!
   
	
	$.widget( "ui.timespinner", $.ui.spinner, {
		options: { step: 1, page: 1 },
		_parse: function( value ) {
			if ( typeof value === "string" ) {
				if ( Number( value ) == value ) {
					return Number( value );
				}
				if(value == '') {
					return null;
				}
				var t = value.split(':', 2);
				var n = (Number(t[0]) * 60 + Number(t[1]));
				return Number(n);
			}
			return value;
		},
		_format: function( value ) {
			if(value == null) {
				return '';
			}
			var v = Number(value);
			while(v < 0) {
				v += (24 * 60);
			}
			v = (v % (24 * 60));
			var mm = "00" + (v % 60);
			mm = mm.substring(mm.length - 2);
			var hh = "00" + (Math.floor(v / 60) % 24);
			hh = hh.substring(hh.length - 2);
			return hh + ":" + mm;
		}
	});
	Globalize.culture("nl");
	$(".time").timespinner();
	
	
})


function correctTime(inputString){
	let hours = parseInt(inputString.substr(0,2));
	let minutes = parseInt(inputString.substr(3,4));
	if(hours > 23 || minutes > 59){
		if(hours > 23){
			inputString = "23" + ":" + (9 > minutes ? "0"+minutes : minutes);
		}
		if(minutes > 59){
			inputString = (9 > hours ? "0"+hours : hours) + ":" + "59";
		}
		if(hours > 23 && minutes > 59){
			inputString = "23:59";
		}
	}
	else if(hours < 0 || minutes < 0){
		if(hours < 0){
			inputString = "00" + ":" + (9 > minutes ? "0"+minutes : minutes);
		}
		if(minutes < 0){
			inputString = (9 > hours ? "0"+hours : hours) + ":" + "00";
		}
		if(hours < 0 && minutes < 0){
			inputString = "00:00";
		}
	}console.log(23);
	return inputString;
}

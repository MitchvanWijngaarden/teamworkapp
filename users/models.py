from django.db import models


# Create your models here.
class User(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    intra_id = models.IntegerField(blank=True, null=True)
    email = models.CharField(db_column='Email', max_length=255, blank=True, null=True)  # Field name made lowercase.
    pw = models.CharField(max_length=255, blank=True, null=True)
    pwhash = models.CharField(db_column='pwHash', max_length=200, blank=True, null=True)  # Field name made lowercase.
    username = models.CharField(max_length=255, blank=True, null=True)
    geslacht = models.CharField(max_length=50, blank=True, null=True)
    bnaam = models.CharField(max_length=50, blank=True, null=True)
    volname = models.CharField(max_length=100, blank=True, null=True)
    roepnaam = models.CharField(max_length=50, blank=True, null=True)
    voorletters = models.CharField(max_length=50, blank=True, null=True)
    tussenvoegsel = models.CharField(max_length=50, blank=True, null=True)
    achternaam = models.CharField(max_length=50, blank=True, null=True)
    functie = models.CharField(max_length=100, blank=True, null=True)
    profiel = models.TextField(blank=True, null=True)
    adres = models.CharField(max_length=50, blank=True, null=True)
    postcode = models.CharField(max_length=50, blank=True, null=True)
    standplaats = models.CharField(max_length=100, blank=True, null=True)
    opleiding = models.CharField(max_length=255, blank=True, null=True)
    hobby = models.CharField(max_length=255, blank=True, null=True)
    matrix = models.SmallIntegerField(blank=True, null=True)
    userid = models.IntegerField(db_column='UserID', blank=True, null=True)  # Field name made lowercase.
    site = models.IntegerField(blank=True, null=True)
    groep_id = models.PositiveIntegerField()
    tel = models.CharField(max_length=50, blank=True, null=True)
    gsm = models.CharField(max_length=50, blank=True, null=True)
    menusize = models.CharField(max_length=1, blank=True, null=True)
    wbm_username = models.CharField(max_length=100, blank=True, null=True)
    wbm_pw = models.CharField(max_length=50, blank=True, null=True)
    wbm_id = models.IntegerField(blank=True, null=True)
    last_logout_date = models.IntegerField()
    last_logout_time = models.IntegerField()
    ipaddress = models.CharField(max_length=20, blank=True, null=True)
    datelastlogin = models.DateTimeField(blank=True, null=True)
    datelastlogout = models.DateTimeField(blank=True, null=True)
    usr_lng = models.PositiveIntegerField(blank=True, null=True)
    wmb_username = models.CharField(max_length=50, blank=True, null=True)
    wmb_pw = models.CharField(max_length=50, blank=True, null=True)
    wmb_id = models.IntegerField(blank=True, null=True)
    show_email = models.PositiveSmallIntegerField()
    show_gsm = models.PositiveSmallIntegerField()
    show_tel = models.PositiveSmallIntegerField()
    show_age = models.PositiveSmallIntegerField()
    show_notification = models.PositiveSmallIntegerField()
    isactive = models.PositiveSmallIntegerField(db_column='isActive')  # Field name made lowercase.
    wgs_uid = models.IntegerField(blank=True, null=True)
    doorstud_studie = models.PositiveIntegerField(blank=True, null=True)
    doorstud_universiteit = models.PositiveIntegerField(blank=True, null=True)
    profile_completed = models.SmallIntegerField(blank=True, null=True)
    doorstud_afstudeerjaar = models.PositiveIntegerField(blank=True, null=True)
    datecreated = models.DateTimeField(blank=True, null=True)
    datemodified = models.DateTimeField(blank=True, null=True)
    dateofbirth = models.DateTimeField(db_column='dateOfBirth', blank=True, null=True)  # Field name made lowercase.
    prefix = models.CharField(max_length=45, blank=True, null=True)
    suffix = models.CharField(max_length=45, blank=True, null=True)
    logincount = models.PositiveIntegerField(db_column='loginCount')  # Field name made lowercase.
    loginfailcount = models.PositiveIntegerField(db_column='loginFailCount')  # Field name made lowercase.
    collapsemenu = models.PositiveSmallIntegerField(db_column='collapseMenu')  # Field name made lowercase.
    profileid = models.IntegerField(db_column='profileId', blank=True, null=True)  # Field name made lowercase.
    website = models.CharField(max_length=45, blank=True, null=True)
    photo = models.CharField(max_length=100, blank=True, null=True)
    thumbnail = models.CharField(max_length=45, blank=True, null=True)
    cv = models.TextField(blank=True, null=True)
    additionalcontactinfo = models.CharField(db_column='additionalContactInfo', max_length=255, blank=True,
                                             null=True)  # Field name made lowercase.
    dateofemployment = models.DateField(db_column='dateOfEmployment', blank=True,
                                        null=True)  # Field name made lowercase.
    expertise = models.CharField(max_length=255, blank=True, null=True)
    keywords = models.CharField(max_length=100, blank=True, null=True)
    skype_id = models.CharField(max_length=50, blank=True, null=True)
    access_token = models.CharField(max_length=50, blank=True, null=True)
    token_secret = models.CharField(max_length=50, blank=True, null=True)
    receive_email = models.SmallIntegerField(blank=True, null=True)
    userextra01 = models.CharField(db_column='userExtra01', max_length=100, blank=True,
                                   null=True)  # Field name made lowercase.
    userextra02 = models.CharField(db_column='userExtra02', max_length=100, blank=True,
                                   null=True)  # Field name made lowercase.
    userextra03 = models.CharField(db_column='userExtra03', max_length=100, blank=True,
                                   null=True)  # Field name made lowercase.
    userextra04 = models.CharField(db_column='userExtra04', max_length=100, blank=True,
                                   null=True)  # Field name made lowercase.
    userextra05 = models.CharField(db_column='userExtra05', max_length=100, blank=True,
                                   null=True)  # Field name made lowercase.
    userextra06 = models.CharField(db_column='userExtra06', max_length=100, blank=True,
                                   null=True)  # Field name made lowercase.
    userextra07 = models.CharField(db_column='userExtra07', max_length=100, blank=True,
                                   null=True)  # Field name made lowercase.
    userextra08 = models.CharField(db_column='userExtra08', max_length=100, blank=True,
                                   null=True)  # Field name made lowercase.
    userextra09 = models.CharField(db_column='userExtra09', max_length=100, blank=True,
                                   null=True)  # Field name made lowercase.
    userextra10 = models.CharField(db_column='userExtra10', max_length=100, blank=True,
                                   null=True)  # Field name made lowercase.
    vacation_days = models.IntegerField(blank=True, null=True)
    manager = models.CharField(max_length=100, blank=True, null=True)
    syncdate = models.DateTimeField()
    privatephone = models.CharField(max_length=50, blank=True, null=True)

    REQUIRED_FIELDS = ()
    USERNAME_FIELD = 'username'
    is_active = True
    is_staff = True

    class Meta:
        db_table = "allmodusers"
        unique_together = ('username', 'intra_id',)

    @property
    def is_anonymous(self):
        """
        Always return False. This is a way of comparing User objects to
        anonymous users.
        """
        return False

    @property
    def is_authenticated(self):
        """
        Always return True. This is a way to tell if the user has been
        authenticated in templates.
        """
        return True

    def has_module_perms(self, app_label):
        return True

    def has_perm(self, perm, obj=None):
        return True

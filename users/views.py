from django.contrib.auth import authenticate, login as f_login
from django.contrib.auth.decorators import login_required
import json
from django.db import connection
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render

# Create your views here.
from django.utils import timezone
from django.views.generic import ListView

from news.forms import UserForm
from team.models import Tmteamsusers
from users.models import User


@login_required
def list_users(request, team_id):
    intra_id = request.user.intra_id

    users = []

    team_users = Tmteamsusers.objects.all().filter(teamid=team_id, intranetid=intra_id)
    for user in team_users:
        user_object = User.objects.get(id=user.userid)
        users.append(user_object)

    context = {'users': users, 'teamid': team_id}
    return render(request, 'users.html', context)


def user_login(request):
    if request.user.is_authenticated:
        query = "select teamid from tmteamsusers where userid=" + str(request.user.id)
        cursor = connection.cursor()
        cursor.execute(query, [])
        row = cursor.fetchone()
        return HttpResponseRedirect('index')
        # return HttpResponseRedirect('/' + str(row[0]))

    if request.method == "POST":
        validate_loginform = UserForm(request.POST)
        if validate_loginform.is_valid():
            user = authenticate(request, username=request.POST.get('username'), password=request.POST.get('password'))
            if user:
                f_login(request, user)
                print("past this?")
                query = "select teamid from tmteamsusers where userid=" + str(user.id)
                cursor = connection.cursor()
                cursor.execute(query, [])
                row = cursor.fetchone()
                data = {'error': False, 'response': 'loggedin successfully', 'url': '/' + str(row[0])}
                return HttpResponse(json.dumps(data))
            else:
                data = {'error': True, 'response': {'all': 'Invalid login details.'}}
                return HttpResponse(json.dumps(data))
        else:
            data = {'error': True, 'response': validate_loginform.errors}
            return HttpResponse(json.dumps(data))
    return render(request, 'user_login.html')


def check_user(request, teamid):
    if request.method == "POST":
        if request.POST.get('username') and request.POST.get('password'):
            users = [(request.POST.get('username')
                      , request.POST.get('password'))
                     ]
            query = "Select * from allmodusers " \
                    "where username=" + request.POST.get('username') + " and pw = " + request.POST.get('password')
            print(query, "******")
            cursor = connection.cursor()
            cursor.executemany(query, users)
            row = cursor.fetchall()
            result = cursor.rowcount
            print(result, "***********************")
            if result:
                for user in row:
                    request.session['userid'] = user.id
                return HttpResponseRedirect('/' + teamid)
            else:
                return HttpResponseRedirect('/' + teamid + '/login')
        else:
            return HttpResponseRedirect('/' + teamid + '/login')


class UserListView(ListView):

    model = User
    paginate_by = 100  # if pagination is desired

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

from django.conf.urls import url
from django.urls import path

from users.views import UserListView
from . import views

urlpatterns = [

    url(r'^$', views.list_users),
    path('checkuser/', views.check_user),
    path('userlist/', UserListView.as_view(template_name='user_list.html')),
    # url(r'^create$', views.create),
    # url(r'^toggle_publish/(?P<editorid>\d+)/$', views.toggle_publish),
    # url(r'^createnews$', views.createnews),
    # url(r'^edit/(?P<editorid>\d+)$', views.edit),
    # url(r'^update/$', views.update),
    # url(r'^delete/(?P<editorid>\d+)$', views.delete),

]
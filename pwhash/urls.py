"""pwhash URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, re_path, include
from django.views.generic import RedirectView

from intra.views import IndexView
from news.views import get_out
from team.views import TestView
from users.views import user_login
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('', RedirectView.as_view(pattern_name='user_login', permanent=False)),
    path('user.login.form', user_login, name="user_login"),
    # path('admin/', admin.site.urls),
    path('index/', IndexView.as_view()),
    re_path('(?P<teamid>\d+)/news/', include('news.urls')),
    re_path('(?P<team_id>\d+)/team', include('team.urls')),
    # (?P<team_name>[a-zA-Z]+)\/team
    re_path('(?P<team_id>\d+)/users/', include('users.urls')),
    path('logout/', get_out, name="get_out"),
    re_path('(?P<team_id>\d+)/test/', TestView.as_view())
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

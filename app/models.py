from django.db import models

'''
ID                     | int(10)              | NO   | PRI | NULL              | auto_increment              |
| intra_id               | int(10)              | YES  | MUL | NULL              |                             |
| Email                  | varchar(255)         | YES  | MUL | NULL              |                             |
| pw                     | varchar(255)         | YES  |     | NULL              |                             |
| pwHash                 | varchar(200)         | YES  |     | NULL              |                             |
| username               | varchar(255)
'''


class Allmodintra(models.Model):
    ID = models.IntegerField(db_column='id', primary_key=True)
    naam = models.TextField()
    salt = models.TextField()
    subdomein = models.CharField(max_length=100)

    class Meta:
        db_table = "allmodintra"

from django.contrib.auth.mixins import UserPassesTestMixin

from team.models import Tmteamsusers, Tmteams
from users.models import User


class TestIfUserInTeam(UserPassesTestMixin):
    def test_func(self):
        team_id = self.kwargs['team_id']
        user_id = self.request.user.id

        try:
            user = User.objects.get(id=user_id)
            team = Tmteams.objects.get(teamid=team_id)
            if team.private:
                return Tmteamsusers.objects.filter(teamid=team_id, userid=user_id, intranetid=user.intra_id).exists()
        except (Tmteams.DoesNotExist, User.DoesNotExist) as e:
            return False
        return True


class TestTeamAdmin(UserPassesTestMixin):
    def test_func(self):
        team_id = self.kwargs['team_id']
        user_id = self.request.user.id

        try:
            user = User.objects.get(id=user_id)
            team_user = Tmteamsusers.objects.get(teamid=team_id, userid=user_id, intranetid=user.intra_id)
            if team_user.isAdministrator:
                return True
        except (User.DoesNotExist, Tmteamsusers.DoesNotExist) as e:
            return False
        return False

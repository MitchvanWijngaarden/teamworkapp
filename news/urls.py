from django.conf.urls import url,include
#from admin.models import user
from . import views

urlpatterns = [
    
    url(r'^$', views.listnews),
	url(r'^create$', views.create),
	url(r'^toggle_publish/(?P<editorid>\d+)/$', views.toggle_publish),
	url(r'^createnews$', views.createnews),
    url(r'^edit/(?P<editorid>\d+)$', views.edit),
	url(r'^update/$', views.update),
	url(r'^delete/(?P<editorid>\d+)$', views.delete),

]
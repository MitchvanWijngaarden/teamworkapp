from django import forms

from users.models import User


class NewsForm(forms.Form):
    titel = forms.CharField(max_length=50000)
    tocus = forms.CharField(max_length=50000, required=False)
    summary = forms.CharField(max_length=50000, required=False)
    datepublished = forms.CharField(max_length=50000)
    categorie = forms.CharField(max_length=50000)
    link = forms.CharField(max_length=50000)
    dateexpired = forms.CharField(max_length=50000)
    timePublished = forms.CharField(max_length=50000)
    timeexpired = forms.CharField(max_length=50000)

    def clean_summaryimage(self):
        print ("hello123.....................")
        print (self, "...................")


class UserForm(forms.ModelForm):
    username = forms.CharField(max_length=50000)
    password = forms.CharField(max_length=50000)

    class Meta:
        model = User
        fields = ('username', )

    def clean_username(self):
        try:
            user = User.objects.filter(username=self.data['username'])
        except:
            user = None
        if user:
            return self.data['username']
        else:
            raise forms.ValidationError('User does not exists with this username')

# Generated by Django 2.1.5 on 2019-11-21 08:14

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='nwscategory',
            fields=[
                ('categoryid', models.IntegerField(db_column='categoryid', primary_key=True, serialize=False, verbose_name=11)),
                ('category', models.CharField(max_length=50)),
                ('fh_url', models.CharField(max_length=255)),
                ('include', models.CharField(max_length=300)),
                ('teamId', models.IntegerField()),
                ('groupid', models.IntegerField()),
                ('zindex', models.IntegerField()),
                ('intraid', models.IntegerField()),
                ('seourls', models.IntegerField()),
                ('notification', models.IntegerField()),
                ('seourl', models.IntegerField()),
                ('isgroupcategory', models.IntegerField()),
                ('description', models.TextField()),
            ],
            options={
                'db_table': 'nwscategory',
            },
        ),
        migrations.CreateModel(
            name='nwseditor',
            fields=[
                ('editorid', models.IntegerField(db_column='editorid', primary_key=True, serialize=False, verbose_name=20)),
                ('intra_id', models.IntegerField()),
                ('tocus', models.TextField()),
                ('titel', models.CharField(max_length=255)),
                ('summary', models.TextField()),
                ('summaryimage', models.CharField(max_length=255)),
                ('fd_date', models.IntegerField()),
                ('fd_time', models.IntegerField()),
                ('datepublished', models.DateTimeField()),
                ('userpublished', models.IntegerField()),
                ('datemodified', models.DateField()),
                ('usermodified', models.IntegerField()),
                ('datecreated', models.DateField()),
                ('usercreated', models.IntegerField()),
                ('publishrss', models.IntegerField()),
                ('dataid', models.IntegerField()),
                ('categorie', models.IntegerField()),
                ('dateexpired', models.DateTimeField()),
                ('allow_reactions', models.IntegerField()),
                ('hide_reactions', models.IntegerField()),
                ('send_notification', models.IntegerField()),
                ('externalid', models.FloatField()),
                ('isvacature', models.IntegerField()),
                ('link', models.CharField(max_length=255)),
                ('isSpotlight', models.IntegerField()),
                ('viewCount', models.IntegerField()),
                ('isRead', models.IntegerField()),
            ],
            options={
                'db_table': 'nwseditor',
            },
        ),
    ]

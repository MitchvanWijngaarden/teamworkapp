import json
import os
import re
from datetime import datetime
from os import listdir
from os.path import isfile, join

from django.conf import settings
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect

from news.models import nwscategory
from .forms import *
from .models import nwseditor


@login_required
def listnews(request, teamid):
    userpublished = request.user.id
    intraid = request.user.intra_id
    categorylist = nwscategory.objects.filter(intraid=intraid, teamId=teamid).values_list('categoryid', flat=True)
    news = nwseditor.objects.filter(categorie__in=categorylist).order_by('-editorid')
    # intraid = '71'
    # queryParam = [(teamid),(intraid)]
    # news = nwseditor.objects.raw('''select * from nwseditor nws
    #                                 inner join nwscategory nc on nc.categoryid = nws.categorie
    #                                 where
    #                                 nc.teamId = %s
    #                                 and
    #                                 nc.intraid = %s''', queryParam )
    context = {'news': news, 'teamid': teamid}
    return render(request, 'news.html', context)


def get_out(request):
    logout(request)
    return HttpResponseRedirect('/')


@login_required
def create(request, teamid):
    intraid = request.user.intra_id
    categorylist = nwscategory.objects.filter(intraid=intraid, teamId=teamid)
    news = nwseditor.objects.filter().order_by('-editorid')

    context = {"teamid": teamid, "categorylist": categorylist, 'news': news}
    return render(request, 'create_news.html', context)


@login_required
def createnews(request, teamid):
    intraid = request.user.intra_id
    userpublished = request.user.ID
    categorylist = nwscategory.objects.filter(intraid=intraid, teamId=teamid)

    news_files = [f for f in listdir(settings.MEDIA_ROOT + "\\" + str(intraid) + "\\teams\\" + str(teamid)) if
                  isfile(join(settings.MEDIA_ROOT + str(intraid) + "\\teams\\" + str(teamid), f))]
    print(news_files, "hello")
    context = {"teamid": teamid, "intraid": intraid, "categorylist": categorylist, 'news_files': news_files}

    if request.method == "POST":
        print("....................", request.POST, request.FILES)
        validate_newsform = NewsForm(request.POST, request.FILES)
        if validate_newsform.is_valid():
            data = request.POST.dict()
            print(data, request.FILES, "..........................")
            import datetime as dt
            pub_time = dt.datetime.strptime(request.POST.get(
                'timePublished').replace(':', ''), '%H%M').time()
            mydatetime = dt.datetime.combine(datetime.strptime(
                request.POST.get('datepublished'), "%d-%m-%Y"), pub_time)
            exp_time = dt.datetime.strptime(request.POST.get(
                'timeexpired').replace(':', ''), '%H%M').time()
            date_exp = dt.datetime.combine(datetime.strptime(
                request.POST.get('dateexpired'), "%d-%m-%Y"), exp_time)
            if request.POST['image_type'] == 'server_image':
                image_full_path = request.POST['image_src']
            else:
                if 'summaryimage' in request.FILES:
                    input_file = request.FILES['summaryimage']
                    image_full_path = write_image_to_file(input_file, intraid, teamid)

            post = nwseditor.objects.create(titel=request.POST.get('titel'), tocus=request.POST.get('tocus'),
                                            intra_id=intraid, summary=request.POST.get('summary'), fd_date=0, fd_time=0,
                                            datepublished=mydatetime, userpublished=userpublished,
                                            datemodified=datetime.now(), usermodified=userpublished,
                                            datecreated=datetime.now(), usercreated=userpublished,
                                            publishrss=0, dataid=0,
                                            categorie=request.POST['categorie'], dateexpired=date_exp,
                                            allow_reactions=0, hide_reactions=0,
                                            externalid=0.0,
                                            isvacature=0, link=request.POST['link'],
                                            isSpotlight=0, viewCount=0,
                                            isRead=0)
            try:
                post.summaryimage = image_full_path
            except NameError:
                print("No image defined.")
            post.save()

            data = {'error': False, 'response': 'News Created Sucessfully', 'url': '/' + str(teamid) + '/news/'}
            return HttpResponse(json.dumps(data), content_type='application/json')
        else:
            data = {'error': True, 'response': validate_newsform.errors}
            return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        return render(request, 'create_news.html', context)


@login_required
def edit(request, editorid, teamid):
    intraid = request.user.intra_id
    categorylist = nwscategory.objects.filter(intraid=intraid, teamId=teamid)
    news = nwseditor.objects.get(editorid=editorid)
    categorylist = nwscategory.objects.filter(teamId=teamid)
    news_files = get_news_article_images(intraid, teamid)
    print(news_files, "hello")

    context = {"news_files": news_files, "intraid": intraid, "news": news, "teamid": teamid,
               "categorylist": categorylist}
    return render(request, 'edit.html', context)


@login_required
def update(request, teamid):
    intraid = request.user.intra_id
    validate_newsform = NewsForm(request.POST, request.FILES)
    if validate_newsform.is_valid():
        if request.POST['image_type'] == 'server_image':
            print(request.POST['image_src'])
            image_full_path = request.POST['image_src']
        else:
            if 'summaryimage' in request.FILES:
                input_file = request.FILES['summaryimage']
                image_full_path = write_image_to_file(input_file, intraid, teamid)


                # file_name = input_file.name.lower()
                # file_name = re.sub('[^a-zA-Z0-9 \n\.]', '', file_name).replace(" ", "-")
                # img_format = file_name.split('.')[-1] if file_name.split('.')[-1] else '.png'
                # path = settings.MEDIA_ROOT + "\\" + str(intraid) + "\\teams\\" + str(teamid)
                # image_path = os.path.join(path, file_name)
                # destination = open(image_path, 'wb+')
                # for chunk in input_file.chunks():
                #     destination.write(chunk)
                # destination.close()
                # image_full_path = image_path.replace(settings.MEDIA_ROOT, '')

        import datetime as dt
        pub_time = dt.datetime.strptime(request.POST.get(
            'timePublished').replace(':', ''), '%H%M').time()
        mydatetime = dt.datetime.combine(datetime.strptime(
            request.POST.get('datepublished'), "%d-%m-%Y"), pub_time)
        exp_time = dt.datetime.strptime(request.POST.get(
            'timeexpired').replace(':', ''), '%H%M').time()
        date_exp = dt.datetime.combine(datetime.strptime(
            request.POST.get('dateexpired'), "%d-%m-%Y"), exp_time)

        editor = nwseditor.objects.filter(editorid=request.POST['editorid']).first()
        if editor:
            editor.titel = request.POST['titel']
            editor.summary = request.POST['summary']
            editor.tocus = request.POST['tocus']
            editor.datepublished = mydatetime
            editor.dateexpired = date_exp
            editor.link = request.POST['link']
            editor.categorie = request.POST['categorie']
            try:
                editor.summaryimage = image_full_path
            except NameError:
                print("No image defined.")
            editor.save()

        data = {'error': False, 'response': 'News Updated Sucessfully', 'url': '/' + str(teamid) + '/news/'}
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        data = {'error': True, 'response': validate_newsform.errors}
        return HttpResponse(json.dumps(data), content_type='application/json')


@login_required
def delete(request, editorid, teamid):
    news = nwseditor.objects.get(editorid=editorid)
    news.delete()

    return redirect('/' + teamid + '/news')


@login_required
def toggle_publish(request, editorid, teamid):
    news = nwseditor.objects.get(editorid=editorid)
    # if news.publishrss == 1:
    #     news.publishrss = 0
    # else:
    #     news.publishrss = 1
    news.publishrss = not news.publishrss
    news.save()
    return redirect('/' + teamid + '/news')


def get_news_article_images(intra_id, team_id):
    return [f for f in listdir(settings.MEDIA_ROOT + "\\" + str(intra_id) + "\\teams\\" + str(team_id)) if
            isfile(join(settings.MEDIA_ROOT + str(intra_id) + "\\teams\\" + str(team_id), f))]


def write_image_to_file(image, intra_id, team_id):
    file_name = re.sub('[^a-zA-Z0-9 \n\.]', '', image.name.lower()).replace(" ", "-")
    file_name.split('.')[-1] if file_name.split('.')[-1] else '.png'
    path = settings.MEDIA_ROOT + "\\" + str(intra_id) + "\\teams\\" + str(team_id)
    image_path = os.path.join(path, file_name)

    if not os.path.exists(path):
        os.makedirs(path)

    destination = open(image_path, 'wb+')
    for chunk in image.chunks():
        destination.write(chunk)
    destination.close()
    return image_path.replace(settings.MEDIA_ROOT, '')

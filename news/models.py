from django.db import models
from users.models import User


class nwseditor (models.Model):
    editorid = models.IntegerField(20, db_column='editorid', primary_key=True)
    intra_id = models.IntegerField()
    tocus = models.TextField()
    titel = models.CharField(max_length=255)
    summary = models.TextField()
    summaryimage = models.CharField(max_length=255)
    fd_date = models.IntegerField()
    fd_time = models.IntegerField()
    datepublished = models.DateTimeField()
    userpublished = models.IntegerField()
    datemodified = models.DateField()
    usermodified = models.IntegerField()
    datecreated = models.DateField()
    usercreated = models.IntegerField()
    publishrss = models.IntegerField()
    dataid = models.IntegerField()
    categorie = models.IntegerField()
    dateexpired = models.DateTimeField()
    allow_reactions = models.IntegerField()
    hide_reactions = models.IntegerField()
    send_notification = models.IntegerField()
    externalid = models.FloatField()
    isvacature = models.IntegerField()
    link = models.CharField(max_length=255)
    isSpotlight = models.IntegerField()
    viewCount = models.IntegerField()
    isRead = models.IntegerField()

    class Meta:
        db_table = "nwseditor"

    def get_author(self):
        return User.objects.get(id=self.userpublished)

    def get_category(self):
        return nwscategory.objects.filter(categoryid=self.categorie)[0].category


class nwscategory (models.Model):
    categoryid = models.IntegerField(11, db_column='categoryid', primary_key=True)
    category = models.CharField(max_length=50)
    fh_url = models.CharField(max_length=255)
    include = models.CharField(max_length=300)
    teamId = models.IntegerField()
    groupid = models.IntegerField()
    zindex = models.IntegerField()
    intraid = models.IntegerField()
    seourls = models.IntegerField()
    notification = models.IntegerField()
    seourl = models.IntegerField()
    isgroupcategory = models.IntegerField()
    description = models.TextField()

    class Meta:
        db_table = "nwscategory"

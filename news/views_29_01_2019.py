from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template.loader import get_template
from django.db import connection
from django.template import loader, Context
from news.models import nwseditor, nwscategory
from django.shortcuts import render, redirect
from datetime import datetime
from django.utils import formats
from django.utils import timezone
from django.contrib.auth import logout
from .models import nwseditor
from django.conf import settings
import os
import re
from django.contrib.auth.decorators import login_required
import json
from .forms import *


@login_required
def listnews(request, teamid):
    userpublished = request.user.ID
    intraid = request.user.intra_id
    categorylist = nwscategory.objects.filter(intraid=intraid, teamId=teamid).values_list('categoryid', flat=True)
    news = nwseditor.objects.filter(categorie__in=categorylist).order_by('-editorid')
    # intraid = '71'
    # queryParam = [(teamid),(intraid)]
    # news = nwseditor.objects.raw('''select * from nwseditor nws
    #                                 inner join nwscategory nc on nc.categoryid = nws.categorie
    #                                 where
    #                                 nc.teamId = %s
    #                                 and
    #                                 nc.intraid = %s''', queryParam )
    context = {'news': news, 'teamid': teamid}
    return render(request, 'news.html', context)


def get_out(request):
    logout(request)
    return HttpResponseRedirect('/')


@login_required
def create(request, teamid):
    intraid = request.user.intra_id
    categorylist = nwscategory.objects.filter(intraid=intraid, teamId=teamid)
    context = {"teamid": teamid, "categorylist": categorylist}
    return render(request, 'create_news.html', context)


@login_required
def createnews(request, teamid):

    t = get_template('create_news.html')
    intraid = request.user.intra_id
    print (request.user.intra_id, "(((((((((((((")
    userpublished = request.user.ID
    categorylist = nwscategory.objects.filter(intraid=intraid, teamId=teamid)
    context = {"teamid": teamid, "categorylist": categorylist}

    if request.method == "POST":
        print("....................", request.POST, request.FILES)
        validate_newsform = NewsForm(request.POST, request.FILES)
        if validate_newsform.is_valid():
            data = request.POST.dict()
            print(data, request.FILES, "..........................")
            import datetime as dt
            pub_time = dt.datetime.strptime(request.POST.get(
                'timePublished').replace(':', ''), '%H%M').time()
            mydatetime = dt.datetime.combine(datetime.strptime(
                request.POST.get('datepublished'), "%d-%m-%Y"), pub_time)
            exp_time = dt.datetime.strptime(request.POST.get(
                'timeexpired').replace(':', ''), '%H%M').time()
            date_exp = dt.datetime.combine(datetime.strptime(
                request.POST.get('dateexpired'), "%d-%m-%Y"), exp_time)
            input_file = request.FILES.get('summaryimage')
            image_path = ''
            if input_file:
                file_name = input_file.name.lower()
                file_name = re.sub('[^a-zA-Z0-9 \n\.]', '', file_name).replace(" ", "-")
                img_format = file_name.split('.')[-1] if file_name.split('.')[-1] else '.png'
                path = settings.BASE_DIR + '/static/'
                image_path = os.path.join(path, file_name)
                destination = open(image_path, 'wb+')
                for chunk in input_file.chunks():
                    destination.write(chunk)
                destination.close()

            post = nwseditor.objects.create(titel=request.POST.get('titel'), tocus=request.POST.get('tocus'),
                                            intra_id=intraid, summary=request.POST.get('summary'), fd_date=0, fd_time=0,
                                            datepublished=mydatetime, userpublished=userpublished,
                                            datemodified=datetime.now(), usermodified=userpublished,
                                            datecreated=datetime.now(), usercreated=userpublished,
                                            publishrss=0, dataid=0,
                                            categorie=request.POST['categorie'], dateexpired=date_exp,
                                            allow_reactions=0, hide_reactions=0,
                                            externalid=0.0,
                                            isvacature=0, link=request.POST['link'],
                                            isSpotlight=0, viewCount=0,
                                            isRead=0)
            if input_file:
                post.summaryimage = image_path.replace(settings.BASE_DIR, '')
                post.save()
            data = {'error': False, 'response': 'News Created Sucessfully', 'url': '/' + str(teamid) + '/news/'}
            return HttpResponse(json.dumps(data), content_type='application/json')
        else:
            data = {'error': True, 'response': validate_newsform.errors}
            return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        return render(request, 'create_news.html', context)


@login_required
def edit(request, editorid, teamid):

    intraid = request.user.intra_id
    categorylist = nwscategory.objects.filter(intraid=intraid, teamId=teamid)
    news = nwseditor.objects.get(editorid=editorid)
    categorylist = nwscategory.objects.filter(teamId=teamid)
    context = {"news": news, "teamid": teamid, "categorylist": categorylist}
    return render(request, 'edit.html', context)


@login_required
def update(request, teamid):
    validate_newsform = NewsForm(request.POST, request.FILES)
    if validate_newsform.is_valid():

        input_file = request.FILES.get('summaryimage')
        image_path = ''
        if input_file:
            file_name = input_file.name.lower()
            file_name = re.sub('[^a-zA-Z0-9 \n\.]', '', file_name).replace(" ", "-")
            img_format = file_name.split('.')[-1] if file_name.split('.')[-1] else '.png'
            path = settings.BASE_DIR + '/static/'
            image_path = os.path.join(path, file_name)
            destination = open(image_path, 'wb+')
            for chunk in input_file.chunks():
                destination.write(chunk)
            destination.close()

        import datetime as dt
        pub_time = dt.datetime.strptime(request.POST.get(
            'timePublished').replace(':', ''), '%H%M').time()
        mydatetime = dt.datetime.combine(datetime.strptime(
            request.POST.get('datepublished'), "%d-%m-%Y"), pub_time)
        exp_time = dt.datetime.strptime(request.POST.get(
            'timeexpired').replace(':', ''), '%H%M').time()
        date_exp = dt.datetime.combine(datetime.strptime(
            request.POST.get('dateexpired'), "%d-%m-%Y"), exp_time)

        editor = nwseditor.objects.filter(editorid=request.POST['editorid']).first()
        if editor:
            editor.titel = request.POST['titel']
            editor.summary = request.POST['summary']
            editor.tocus = request.POST['tocus']
            editor.datepublished = mydatetime
            editor.dateexpired = date_exp
            editor.link = request.POST['link']
            editor.categorie = request.POST['categorie']
            if input_file:
                editor.summaryimage = image_path.replace(settings.BASE_DIR, '')
            editor.save()

        data = {'error': False, 'response': 'News Updated Sucessfully', 'url': '/' + str(teamid) + '/news/'}
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        data = {'error': True, 'response': validate_newsform.errors}
        return HttpResponse(json.dumps(data), content_type='application/json')


@login_required
def delete(request, editorid, teamid):
    news = nwseditor.objects.get(editorid=editorid)
    news.delete()
    return redirect('/'+teamid+'/news')
